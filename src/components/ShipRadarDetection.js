import React, { useState } from "react";
/*import ReactDOM from "react-dom";*/

function RadarDetection() {

    const [range, setRange] = useState("8000");
    const [shipSignature, setRadarSignature] = useState("3783");
    const [RadarStatus, setRadarStatus] = useState("0");
    const [CommsStatus, setCommsStatus] = useState("0");

    const parallax = {name : "RM50 Parallax", radiatedPower: 4100, gain: 50, apertureSize: 30, sensitivity: -38.5};
    const frontline = {name : "RS35 Frontline", radiatedPower: 3500, gain: 40, apertureSize: 25, sensitivity: -36};
    const spyglass = {name : "RS41 Spyglass", radiatedPower: 4500, gain: 60, apertureSize: 40, sensitivity: -38.5};

    const [selectedRadar, setSelectedRadar] = useState(false);

    const noise = 0.0000001;
    const pi = Math.PI;

    /*
    Impact power = ((Radiated power*gain)/(4*pi*range^2))/noise
    Returned power = (((Impact power*signature)*gain)/(4*pi*range^2))*aperture size
    Signal Loss = log10(Returned power*noise/0.001)*10
    result : If Returned Power < 1 then not enough power, if Signal Loss < Sensivity then not enough sensivity
    https://www.radartutorial.eu/06.antennas/Antenna%20Characteristics.en.html
    */

    let shipFinalSignature = shipSignature-(RadarStatus*shipSignature)-(CommsStatus*shipSignature)

    let radarImpactPower = ((selectedRadar.radiatedPower*selectedRadar.gain)/(4*pi*range*range))/noise
    let radarReturnedPower = (((radarImpactPower*(shipFinalSignature/10))*selectedRadar.gain)/(4*pi*range*range))*selectedRadar.apertureSize
    let radarSignalLoss = Math.log10(radarReturnedPower*noise/0.001)*10
    let radarResult = "UNKNOWN"
    if (radarReturnedPower < 1) {
        radarResult = "NOT ENOUGH POWER"
    } else if (radarSignalLoss < selectedRadar.sensitivity) {
        radarResult = "NOT ENOUGH SENSIVITY"
    } else {
        radarResult = "SHIP DETECTED"
    }

    const handleSignatureChange = (event) => {
        setRadarSignature(event.target.value);
    }

    const handleRangeChange = (event) => {
        setRange(event.target.value);
    }

    const handleRadarStatusChange = (event) => {
        setRadarStatus(event.target.value);
    }

    const handleCommunicationsStatusChange = (event) => {
        setCommsStatus(event.target.value);
    }

    return (
        <>

            <h2 class="centered-text">Radar detection</h2>
            
            <div class="grey-border" width="100%">
                <h3>Radar signature : {shipSignature} m²</h3>

                <input type="radio" id="corvette" name="radarsignature"  value="2980.16" onChange={handleSignatureChange} />
                <label for="corvette">Sprinter (Corvette) : 2980.16 m²</label><br />

                <input type="radio" id="frigate" name="radarsignature" value="3783" onChange={handleSignatureChange} />
                <label for="frigate">Raines (Frigate) : 3783 m²</label><br />

                <input type="radio" id="destroyer" name="radarsignature" value="6281.85" onChange={handleSignatureChange} />
                <label for="destroyer">Keystone (Destroyer) : 6281.85 m²</label><br />

                <input type="radio" id="lightcruiser" name="radarsignature" value="11727.54" onChange={handleSignatureChange} />
                <label for="lightcruiser">Vauxhall (Light Cruiser) : 11727.54 m²</label><br />

                <input type="radio" id="heavycruiser" name="radarsignature" value="14332.5" onChange={handleSignatureChange} />
                <label for="heavycruiser">Axford (Heavy Cruiser) : 14332.5 m²</label><br />

                <input type="radio" id="battleship" name="radarsignature" value="27876" onChange={handleSignatureChange} />
                <label for="battleship">Solomon (Battleship) : 27876 m²</label><br />
                
                <h3>Range : {range} m</h3>
                <input type="range" class="radar-range" min="100" max="18000" step="100" value={range} onChange={handleRangeChange}/>

                <h3>Radar status : </h3>
                <input type="radio" id="radarenabled" name="radarstatus" value="0" onChange={handleRadarStatusChange} />
                <label for="radarenabled">ON</label><br />

                <input type="radio" id="radardisabled" name="radarstatus" value="0.15" onChange={handleRadarStatusChange} />
                <label for="radarenabled">OFF (-15% signature)</label><br />

                <h3>Communications status : </h3>
                <input type="radio" id="radarenabled" name="commsstatus" value="0" onChange={handleCommunicationsStatusChange} />
                <label for="radarenabled">XMIT</label><br />

                <input type="radio" id="radardisabled" name="commsstatus" value="0.1" onChange={handleCommunicationsStatusChange} />
                <label for="radarenabled">RECV (-10% signature)</label><br />

            </div>

            <div>
                <h2 class="centered-text grey-border">Results</h2>

                <h3>Select radar :</h3>
                <button onClick={() => setSelectedRadar(parallax)}>Parallax</button>
                <button onClick={() => setSelectedRadar(frontline)}>Frontline</button>
                <button onClick={() => setSelectedRadar(spyglass)}>Spyglass</button>

                {selectedRadar && (
                <div class="big-div">
                    <h3 class="centered-text">{selectedRadar.name}</h3>
                    <li class="grey-border">
                        <ul>
                            Radiated power : {selectedRadar.radiatedPower} kW
                        </ul>
                        <ul>
                            Gain : {selectedRadar.gain} dB
                        </ul>
                        <ul>
                            Aperture size : {selectedRadar.apertureSize} m²
                        </ul>
                        <ul>
                            Sensitivity : {selectedRadar.sensitivity} dBm
                        </ul>

                    </li>
                    <li>
                        <ul>
                            Impact power : {radarImpactPower} kW
                        </ul>
                        <ul>
                            Returned power : {radarReturnedPower} dB
                        </ul>
                        <ul>
                            Signal loss : {radarSignalLoss} m²
                        </ul>
                    </li>
                    <h3 class="centered-text">RESULT : {radarResult}</h3>
                </div>
                )}

            </div>
            
        </>
    )
}

export default RadarDetection;