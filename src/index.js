import React, { useState, /*createContext, useContext*/ } from 'react';
import ReactDOM from 'react-dom';
import './main.css';
import ShipCard from './components/ShipCard';
import ShipComponents from './components/ShipComponents';
import ShipAdvancedStats from './components/ShipAdvancedStats';
import ShipStats from './components/ShipStats';

/*
Class :
0 : Sprinter Corvette (FFL)
1 : Raines Frigate (FF, FFG, FFE)
2 : Keystone Destroyer (DD, DDG, DDE)
3 : Vauxhall Light Cruiser (CL)
4 : Axford Heavy Cruiser (CH)
5 : Solomon Battleship (BB)
https://en.wikipedia.org/wiki/Hull_classification_symbol

Assets :
    Sprinter Corvette :
        Mounting : 4 {3x4x5, 3x4x3, 2x2x2, 2x2x2}
        Compartment : 4 {4x1x6, 4x1x6, 3x1x3, 3x1x3}
        Module : 4 {3x3x3, 8x3x6, 2x2x2, 2x2x2}

    Raines Frigate :
        Mounting : 4 {3x4x3, 3x4x3, 3x4x3, 3x4x3}
        Compartment : 5 {4x1x6, 4x1x6, 3x1x3, 3x1x3, 3x1x5}
        Module : 5 {6x6x6, 8x3x6, 2x2x2, 2x2x2, 3x3x3}

    Keystone Destroyer :
        Mounting : 7 {3x4x5, 3x4x5, 3x4x3, 3x4x3, 2x2x2, 2x2x2, 4x12x4}
        Compartment : 7 {6x1x8, 6x1x8, 6x1x4, 6x1x4, 6x1x4, 6x1x4, 6x1x4}
        Module : 7 {6x6x6, 8x8x6, 3x3x3, 3x3x3, 2x2x2, 2x2x2, 3x3x3}

    Vauxhall Light Cruiser :
        Mounting : 10 {6*(3x4x5), 4*(2x2x2)}
        Compartment : 9 {4*(6x1x8), 6x1x4, 6x1x4, 4x1x8, 6x1x4, 6x1x8}
        Module : 9 {6x6x6, 3x3x3, 8x12x10, 3x3x3, 2x2x2, 3x3x3, 2x2x2, 2x2x2, 2x2x2}

    Axford Heavy Cruiser :
        Mounting : 11 {6x4x6, 6x4x6, 8x7x8, 4*(3x4x3), 4*(2x2x2)}
        Compartment : 11 {4*(6x1x8), 6x1x4, 6x1x4, 4x1x6, 6x1x6, 6x1x6, 6x1x8, 4x1x6}
        Module : 12 {6x6x6, 6x6x6, 8x12x10, 3x3x3, 2x2x2, 3x3x3, 2x2x2, 2x2x2, 2x2x2}
*/

function Fleet(props) {
    const [shipIsShown, setAddshipIsShown] = useState(false);

    const defaultCorvette = {id: 4, classID: 0, name:"Default", hullNumber:"1", symbolOption: 0, mounting: {}, compartment: {}, module: {}}
    const defaultFrigate = {id: 5, classID: 1, name:"Default", hullNumber:"10", symbolOption: 0, mounting: {}, compartment: {}, module: {}}
    const defaultDestroyer = {id: 6, classID: 2, name:"Default", hullNumber:"11", symbolOption: 0, mounting: {}, compartment: {}, module: {}}
    const defaultLightCruiser = {id: 7, classID: 3, name:"Default", hullNumber:"100", symbolOption: 0, mounting: {}, compartment: {}, module: {}}
    const defaultHeavyCruiser = {id: 8, classID: 4, name:"Default", hullNumber:"101", symbolOption: 0, mounting: {}, compartment: {}, module: {}}
    const defaultBattleship = {id: 9, classID: 5, name:"Default", hullNumber:"110", symbolOption: 0, mounting: {}, compartment: {}, module: {}}

    return (
        <>
            <h1>Fleet "{props.fleetName}" ({props.fleet.length} ships)</h1>
            <button >Import fleet XML</button>
            <button >Export fleet XML</button>
            <button >Import ship XML</button>
            <button >Export ship XML</button>
            <div className={"big-div"}>
                {props.fleet.map((ship) => <ShipCard key={ship.id} classID={ship.classID} name={ship.name} hullNumber={ship.hullNumber} symbolOption={ship.symbolOption} selectedShip={props.selectedShip}/>)}
            </div>
            <button onClick={() => setAddshipIsShown(true)}>Add Ship</button>
            {shipIsShown && (
                <div>
                    <button onClick={() => {props.fleet.push(defaultCorvette); setAddshipIsShown(false)}}>Add Corvette</button>
                    <button onClick={() => {props.fleet.push(defaultFrigate); setAddshipIsShown(false)}}>Add Frigate</button>
                    <button onClick={() => {props.fleet.push(defaultDestroyer); setAddshipIsShown(false)}}>Add Destroyer</button>
                    <button onClick={() => {props.fleet.push(defaultLightCruiser); setAddshipIsShown(false)}}>Add Light Cruiser</button>
                    <button onClick={() => {props.fleet.push(defaultHeavyCruiser); setAddshipIsShown(false)}}>Add Heavy Cruiser</button>
                    <button onClick={() => {props.fleet.push(defaultBattleship); setAddshipIsShown(false)}}>Add Battleship</button>
                    <button onClick={() => setAddshipIsShown(false)}>Cancel</button>
                </div>
            )}
        </>
    )
}

function Shipyard(props) {  
    return (
      <>
        {props.selectedShip === "" ? <h1>No ship selected</h1> : <h1>Selected ship is {props.selectedShip}!</h1>}
        <div className={"big-div"}>
        <div class="row">
            <div class="column left grey-border">
                <ShipComponents />
            </div>
            <div class="column middle grey-border">
                <ShipAdvancedStats />
            </div>
            <div class="column right grey-border">
                <ShipStats />
            </div>
        </div>
        </div>
      </>
    );
}

function MainBoard() {
    let defaultFleetName = "Feldspar Squadron"
    let selectedShip = ""
    let fleet = [
        {id: 0, classID: 3, name:"Akula", hullNumber:"335", symbolOption: 0, mounting: {}, compartment: {}, module: {}},
        {id: 1, classID: 4, name:"Insatian", hullNumber:"707", symbolOption: 0, mounting: {}, compartment: {}, module: {}},
        {id: 2, classID: 1, name:"Mal0", hullNumber:"1471", symbolOption: 1, mounting: {}, compartment: {}, module: {}}
    ];

    return (
        <>
            <Fleet
                fleetName={defaultFleetName}
                fleet={fleet}
                selectedShip={selectedShip}
            />
            <Shipyard
                fleet={fleet}
                selectedShip={selectedShip}
            />
        </>
    )
}

ReactDOM.render(
    <React.StrictMode>
        <MainBoard />
    </React.StrictMode>
    , document.getElementById('root')
);