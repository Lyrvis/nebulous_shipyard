import React, { useState, createContext, useContext } from 'react';

import pictureSprinter from '../img/sprinter.png';
import pictureRaines from '../img/raines.png';
import pictureKeystone from '../img/keystone.png';
import pictureVauxhall from '../img/vauxhall.png';
import pictureAxford from '../img/axford.png';
import pictureSolomon from '../img/solomon.png';
import pictureUnknown from '../img/unknown.svg';

/*
Class :
0 : Sprinter Corvette (FFL)
1 : Raines Frigate (FF, FFG, FFE)
2 : Keystone Destroyer (DD, DDG, DDE)
3 : Vauxhall Light Cruiser (CL)
4 : Axford Heavy Cruiser (CH)
5 : Solomon Battleship (BB)
https://en.wikipedia.org/wiki/Hull_classification_symbol
*/

function Ship(props) {
    let className = "Unknown";
    let hullClass = "XX";
    let picture = "../img/unknown.svg"
    
    switch (props.classID) { //Set picture, class name, and hull class
        case 0:            
            picture = pictureSprinter
            className = "Sprinter (Corvette)"
            hullClass = "FFL"
            break
        case 1:
            picture = pictureRaines
            className = "Raines (Frigate)"
            switch (props.symbolOption) {
                case 0:
                    hullClass = "FF"
                    break
                case 1:
                    hullClass = "FFG"
                    break
                case 2:
                    hullClass = "FFE"
                    break
            }
            break
        case 2:
            picture = pictureKeystone
            className = "Keystone (Destroyer)"
            switch (props.symbolOption) {
                case 0:
                    hullClass = "DD"
                    break
                case 1:
                    hullClass = "DDG"
                    break
                case 2:
                    hullClass = "DDE"
                    break
            }
            break
        case 3:
            picture = pictureVauxhall
            className = "Vauxhall (Light Cruiser)"
            hullClass = "CL"
            break
        case 4:
            picture = pictureAxford
            className = "Axford (Heavy Cruiser)"
            hullClass = "CH"
            break
        case 5:
            picture = pictureSolomon
            className = "Solomon (Battleship)"
            hullClass = "BB"
            break
        default:
            picture = pictureUnknown
            className = "Unknown"
            hullClass = "XX"
            break
    }

    return (
        <>
        <button type="button" onClick={() => props.selectedShip = props.shipID}>
            <h2>{className}</h2>
            <h3>"{props.name}" - {hullClass}-{props.hullNumber}</h3>
            <img src={picture} alt={className} draggable="false" width="256" height="128"></img>
        </button>
        </>
    )
}

export default Ship;