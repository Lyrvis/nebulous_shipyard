import React, { useState, createContext, useContext } from 'react';
import ReactDOM from 'react-dom';
import Ship from './ShipCard';

function ShipComponents() {

    return (
        <>
            <div>
                <h2>
                    Mounting :
                </h2>
            </div>
            <button>Mounting</button>
            <div>
                <h2>
                    Compartment :
                </h2>
            </div>
            <button>Compartment</button>
            <div>
                <h2>
                    Module :
                </h2>
            </div>
            <button>Module</button>
        </>
    )
}

export default ShipComponents;