import RadarDetection from './ShipRadarDetection';

function AdvancedStats() {

    return (
        <>
        <h1>Advanced stats</h1>
        
        <RadarDetection />

        </>
    )
}

export default AdvancedStats;