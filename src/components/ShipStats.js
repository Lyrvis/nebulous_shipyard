import React from "react";
import ReactDOM from "react-dom";

function Stats() {

    return (
        <>
            <h2 class="centered-text">Ship stats</h2>
            <div class="grey-border">
                <li>
                    <ul>
                        Hull
                    </ul>
                    <ul>
                        Manning
                    </ul>
                    <ul>
                        Propulsion
                    </ul>
                    <ul>
                        Ressources
                    </ul>
                    <ul>
                        Sensors
                    </ul>
                    <ul>
                        Damage Control
                    </ul>
                    <ul>
                        Communications
                    </ul>
                    <ul>
                        Weapons
                    </ul>
                </li>
            </div>
        </>
    )
}

export default Stats;